/*
- Clone the given repository in your s64 folder.
- Rename the cloned folder as activity
- Execute command: npm i inside the active folder to install the dependencies.
- Try npm test command to run the test.
- Create your function coding solution in queue.js
- DO NOT refactor the test.js file.
- DO "npm test" every time there is a change in the code that needed to be tested.
- Create a new personal repo folder named s64 and create a repo project named activity inside and push your mock-tech exam
*/

let collection = [];

// Write the queue functions below.
let first = 0;          
let last = -1;        

const print = () => {
    return collection;
}

const enqueue = (item) => {
  last++;
  collection[last] = item;
  return collection;
}

const dequeue = () => {
  if (first > last) {
    return null;
  }
  let newArray = [];
  for(let i=0; i<collection.length-1; i++){
    newArray[i] = collection[i+1];
  }
  collection = newArray;
  last--;
  return collection;
}

const front = () => {
    return collection[first];
}

const size = () => {
    return collection.length;
}

const isEmpty = () => {
    return collection.length === 0;
}



module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};